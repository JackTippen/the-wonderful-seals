package org.domco.connectfour.console.presentation;

import org.domco.connectfour.game.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Allows console ui to interact with the game service
 */
class PlayUserMove {

    /**
     * Log.
     */
    private static final Logger logger = LoggerFactory.getLogger(PlayUserMove.class);

    /**
     * Current state of the game.
     */
    private GameState currentGameState;

    /**
     * Boolean to track if the user's move was accepted by the game move service.
     */
    private boolean gameStateSuccessfullyUpdated;

    /**
     * Constructor to create an initial game state before the first move is played.
     *
     * @param gameParameters User's choice for the game parameters
     */
    PlayUserMove(GameParameters gameParameters) {
        currentGameState = new GameState.GameStateBuilder()
                .withGameWidth(gameParameters.getGameWidth())
                .withGameHeight(gameParameters.getGameHeight())
                .withConsecutivePiecesForWin(gameParameters.getConsecutivePiecesForWin())
                .withPlayerTwoHandicap(gameParameters.getPlayerTwoHandicap())
                .withColourPlayed(PieceColour.YELLOW)
                .withGamePieceSet(new HashSet<>())
                .build();
    }

    /**
     * Take user input, convert to a game move, pass to the service and update the game move on the ui
     *
     * @param userMove user input of the column they want to play their piece in
     * @return updated game grid.
     */
    String playMove(int userMove) {

        logger.debug("Beginning to play user's chosen move.");

        GameState updatedGameState = createEndGameState(userMove);

        //Flag to tell the Game class if the user's move was accepted or rejected.
        gameStateSuccessfullyUpdated = !updatedGameState.equals(currentGameState);

        // Update current game state to reflect changes from the service attempting to play the user's move.
        currentGameState = updatedGameState;

        return createConsoleGridOfGameState(updatedGameState);

    }

    /**
     * @param userMove int value of the column the user wants to play in.
     * @return endGameState to use in the game move based on the user's chosen column.
     */
    private GameState createEndGameState(int userMove) {

        logger.debug("Calculating the moves that can be played");

        final Set<GameMove> availableGameMoves = currentGameState.calculateAvailableMoves();

        final Optional<GameMove> gameMoveOptional = availableGameMoves.stream()
                .filter(move -> move.getPlayedPiece().getxPosition() == userMove).findFirst();

        GameState acceptedEndGameState;

        if (gameMoveOptional.isPresent()) {
            acceptedEndGameState = gameMoveOptional.get().getEndGameState();
        } else {
            acceptedEndGameState = currentGameState;
        }

        logger.debug("Accepted end game state found: {}", acceptedEndGameState);

        return acceptedEndGameState;

    }

    /**
     * Update the console view with the new game move.
     *
     * @param newGameState end game state of the new game move.
     * @return updated view for the console to display
     * How the game state numbers match up to the StringBuilder
     * 6 |   |   |   |   |   |      |     |
     * 5 |   |   |   |   |   |      |     |
     * 4 |   |   |   |   |   |      |     |
     * 3 |   |   |   |   |   |      |     |
     * 2 |   |   |   |   |   |      |     |
     * 1 |   |   |   | r |   |      |     |
     * 0   --------------------------------
     * ..0 1 2 3 4 5 6 7 8 9 10 11 12 13 14
     */
    String createConsoleGridOfGameState(GameState newGameState) {

        logger.debug("Creating the console game grid based on the newGameState.");

        int gameGridLength = (2 * newGameState.getGameWidth()) + 1;
        int gameGridHeight = newGameState.getGameHeight();

        StringBuilder sb = new StringBuilder();

        //Looping over each row in the grid.
        for (int row = gameGridHeight; row >= 0; row--) {

            //Looping over each column in the grid.
            for (int column = 0; column < gameGridLength; column++) {

                //Determine if this is a playable position, a column separator, or the bottom of the grid.
                if (row == 0) {
                    sb.append("-");
                } else if (column % 2 == 0) {
                    sb.append("|");
                } else {

                    //Create the possible game pieces that could be present in this cell
                    //map console view gird positions to actual piece positions
                    GamePiece redGamePiece = new GamePiece.GamePieceBuilder()
                            .withxPosition((column - 1) / 2)
                            .withyPosition(row - 1)
                            .withPieceColour(PieceColour.RED)
                            .build();

                    GamePiece yellowGamePiece = new GamePiece.GamePieceBuilder()
                            .withxPosition((column - 1) / 2)
                            .withyPosition(row - 1)
                            .withPieceColour(PieceColour.YELLOW)
                            .build();

                    if (newGameState.getGamePieceSet().contains(redGamePiece)) {
                        sb.append("R");
                    } else if (newGameState.getGamePieceSet().contains(yellowGamePiece)) {
                        sb.append("Y");
                    } else {
                        sb.append(" ");
                    }
                }

                // Start a new line of we reach the end of the row.
                if (column == gameGridLength - 1) {
                    sb.append("\n");
                }

            }

        }

        logger.debug("Returning the updated console game grid.");

        return sb.toString();

    }

    /**
     * Calculates if the most recent game state is a winning state or not.
     *
     * @return GameStatus
     */
    GameStatus getGameStatus() {
        return currentGameState.getGameStatus();
    }

    /**
     * Getter for the current game state.
     *
     * @return current game state
     */
    GameState getCurrentGameState() {
        return currentGameState;
    }

    /**
     * Getter for the game state successfully updated flag
     *
     * @return boolean for the user's move being accepted by the service.
     */
    boolean isGameStateSuccessfullyUpdated() {
        return gameStateSuccessfullyUpdated;
    }

}