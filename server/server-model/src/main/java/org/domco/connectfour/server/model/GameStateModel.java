package org.domco.connectfour.server.model;

import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Model for the Game State
 */
@ApiModel(description = "Stores game and player data.")
public final class GameStateModel {

    @ApiModelProperty("Player name.")
    private String playerName;

    @ApiModelProperty("Opponent name.")
    private String opponentName;

    @ApiModelProperty("Player colour.")
    private String playerColour;

    @ApiModelProperty("Opponent colour.")
    private String opponentColour;

    @ApiModelProperty("The colour to play next.")
    private String turnColour;

    @ApiModelProperty(value = "Status of game, used to determine if it has been finished.", allowableValues = "RED_WIN, YELLOW_WIN, UNDETERMINED, TIE")
    private String gameStatus;

    @ApiModelProperty("Name of the winner. Will be null if the game is still in progress or tied.")
    private String winner;

    @ApiModelProperty("Width of the game.")
    private int gameWidth;

    @ApiModelProperty("Height of the game.")
    private int gameHeight;

    @ApiModelProperty("List of pieces in the game. The game can be rendered from this list.")
    private Set<GamePieceModel> gamePieceSet;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(final String playerName) {
        this.playerName = playerName;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public void setOpponentName(final String opponentName) {
        this.opponentName = opponentName;
    }

    public String getPlayerColour() {
        return playerColour;
    }

    public void setPlayerColour(final String playerColour) {
        this.playerColour = playerColour;
    }

    public String getOpponentColour() {
        return opponentColour;
    }

    public void setOpponentColour(final String opponentColour) {
        this.opponentColour = opponentColour;
    }

    public String getTurnColour() {
        return turnColour;
    }

    public void setTurnColour(final String turnColour) {
        this.turnColour = turnColour;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(final String gameStatus) {
        this.gameStatus = gameStatus;
    }

    public int getGameWidth() {
        return gameWidth;
    }

    public void setGameWidth(final int gameWidth) {
        this.gameWidth = gameWidth;
    }

    public int getGameHeight() {
        return gameHeight;
    }

    public void setGameHeight(final int gameHeight) {
        this.gameHeight = gameHeight;
    }

    public Set<GamePieceModel> getGamePieceSet() {
        return gamePieceSet;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(final String winner) {
        this.winner = winner;
    }

    public void setGamePieceSet(final Set<GamePieceModel> gamePieceSet) {
        this.gamePieceSet = gamePieceSet;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gameWidth).append(gameHeight).append(gamePieceSet).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals = false;
        if (obj instanceof GameStateModel) {
            final GameStateModel rhs = (GameStateModel) obj;
            equals = new EqualsBuilder().append(gameWidth, rhs.gameWidth)
                    .append(gameHeight, rhs.gameHeight)
                    .append(gamePieceSet, rhs.gamePieceSet)
                    .isEquals();
        }
        return equals;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gameWidth", gameWidth)
                .append("gameHeight", gameHeight)
                .append("gamePieceSet", gamePieceSet)
                .toString();
    }
}
