package org.domco.connectfour.server.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Model for the Game Piece
 */
@ApiModel(description = "Contains game piece data.")
public final class GamePieceModel {

    @ApiModelProperty("Horizontal position. 0 is the far left of the grid.")
    private int xPosition;

    @ApiModelProperty("Vertical position. 0 is the botton of the grid.")
    private int yPosition;

    @ApiModelProperty(value = "Colour of the piece.", allowableValues = "RED, YELLOW")
    private String pieceColour;

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(final int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(final int yPosition) {
        this.yPosition = yPosition;
    }

    public String getPieceColour() {
        return pieceColour;
    }

    public void setPieceColour(final String pieceColour) {
        this.pieceColour = pieceColour;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(xPosition).append(yPosition).append(pieceColour).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals = false;
        if (obj instanceof GamePieceModel) {
            final GamePieceModel rhs = (GamePieceModel) obj;
            equals = new EqualsBuilder().append(xPosition, rhs.xPosition)
                    .append(yPosition, rhs.yPosition)
                    .append(pieceColour, rhs.pieceColour)
                    .isEquals();
        }
        return equals;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("xPosition", xPosition)
                .append("yPosition", yPosition)
                .append("pieceColour", pieceColour)
                .toString();
    }

}
