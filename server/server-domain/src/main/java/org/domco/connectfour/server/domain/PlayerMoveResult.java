package org.domco.connectfour.server.domain;

public enum PlayerMoveResult {

    SUCCESS,

    NOT_PLAYERS_TURN,

    MOVE_NOT_AVAILABLE,

    MOVE_OUT_OF_BOUNDS,

    GAME_OVER,

    GAME_NOT_READY

}
