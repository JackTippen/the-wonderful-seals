package org.domco.connectfour.server.ui.constants;

/**
 * Class for all constants of the connect4 application
 */
public class Constant {

    public final static String BASE_URI = "http://localhost:8080";

    public final static String RED = "red";
    public final static String YELLOW = "yellow";
    public final static String UNDEFINED = "undefined";

    public final static String YELLOW_WIN = "yellowWin";
    public final static String RED_WIN = "redWin";
    public final static String UNDETERMINED = "undetermined";
    public final static String TIE = "tie";

    public final static String RED_COLUMN_SYMBOL = "R";
    public final static String YELLOW_COLUMN_SYMBOL = "Y";

    public final static String TITLE_TEXT = "Connect Four.";
    public final static String WAITING_MESSAGE = "Waiting for a game...";
    public final static String DROP_MESSAGE = "Drop Drop Drop Drop Drop Drop Drop";

}
