package org.domco.connectfour.server.ui.stepdefs;

import com.frameworkium.core.ui.tests.BaseUITest;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.domco.connectfour.server.ui.presentation.service.BrowserInteractionService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Wait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.management.JMException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Setup class for UI tests to initialise driver and browser instances
 * and remove test data from previous tests.
 */
public class CucumberUITest extends BaseUITest {

    /** Logger. */
    private static final Logger logger = LoggerFactory.getLogger(CucumberUITest.class);

    private final BrowserInteractionService browserInteractionService;

    @Inject
    public CucumberUITest(BrowserInteractionService browserInteractionService) {
        this.browserInteractionService = browserInteractionService;
    }

    @Before
    public void setup() {

        String geckoDriver = System.getProperty("webdriver.gecko.driver");

        /*
        This block checks if a webdriver path is specified and attempt to find one if not.
        Ideally, something in the Gradle stack would've found this already, but IDEs tend not go through this route and
        goes straight to the code.
        Instead, this code will run the Gradle configureGeckoDriverBinary task in another process, then use a regular
        expression to parse out the driver path.

        This isn't the worst thing I've ever done, but it has pushed that time I threw up on my dog out of the top 5. I
        hope someone better than me can find a better solution but I've reached the end of the flowchart and now Madness
        rules. Some notes for brave Java pioneers of the future:

            * Yes, you can specify -Dwebdriver.gecko.driver in the IDE VM args, but that's extra config.
            * The IDEA plugins (from both Intellij and erdi) seem promising and might work one day. However, currently
              they hinge on extending the junit phases and Cucumber lives completely separately from the lifecycle. I
              was not able to find a programmatic way to hook into the Cucumber tasks.
            * These driver paths could be hard coded (that's what we did at first) and track web drivers in the repoo,
              but having a binary file for every browser for every OS seemed somehow more sick and wrong than this
              heinous solution.

        Good luck noble coders. I hope you are stronger than me and do not fall to Lord Hack as quickly as I did.
         */
        if (StringUtils.isEmpty(geckoDriver)) {
            try {
                Process process;
                if (SystemUtils.IS_OS_WINDOWS) {
                    process = Runtime.getRuntime().exec("cmd /c gradlew.bat configureGeckoDriverBinary");
                }
                else {
                    process = Runtime.getRuntime().exec("sh gradlew configureGeckoDriverBinary");
                }
                process.waitFor();
                String output = IOUtils.toString(process.getInputStream(), Charset.defaultCharset());

                Pattern driverPathPattern = Pattern.compile("(?<=driver-path:)[^\\s]+");
                Matcher driverPathMatcher = driverPathPattern.matcher(output);
                if (driverPathMatcher.find()) {
                    geckoDriver = driverPathMatcher.group();
                }
            } catch (IOException | InterruptedException e) {
                logger.error("Cannot find webdriver path.", e);
            }
        }

        if (StringUtils.isEmpty(geckoDriver)) {
            throw new IllegalStateException("Cannot determine path for the Gecko webdriver. Specify it manually by " +
                    "adding a VM argument: -Dwebdriver.gecko.driver=path/to/driver");
        }

        System.out.println("Webdriver: " + geckoDriver);
        System.setProperty("webdriver.gecko.driver", geckoDriver);
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", true);

        //Clean up any leftover players from the previous test
        removePreviousTestPlayers();

        initialiseDriverPool();
        instantiateDriverObject();

        setDriverAndWaitInBrowserInteractionService(getDriver(), getWait());
    }

    @After
    public void tearDown() {
        tearDownSuite();
        shutdownScreenshotExecutor();
        tearDownBrowser();
        getDriver().quit();
    }

    /**
     * Sets the driver in the browser interaction service
     *
     * @param driver to interact with the browser during tests
     */
    private void setDriverAndWaitInBrowserInteractionService(WebDriver driver, Wait<WebDriver> wait) {
        this.browserInteractionService.setDriver(driver);
        this.browserInteractionService.setWait(wait);
    }

    /**
     * Manages the JMX agent to wipe all existing players before the test runs.
     */
    private void removePreviousTestPlayers() {

        try {

            final JMXServiceURL url = new JMXServiceURL(
                    "service:jmx:rmi:///jndi/rmi://localhost:9004/jmxrmi");
            final JMXConnector jmxc = JMXConnectorFactory.connect(url, null);

            final MBeanServerConnection mbsx = jmxc.getMBeanServerConnection();

            // Uniquely identify the MBeans and register them with the platform MBeanServer
            final ObjectName playerLookupManagerName = new ObjectName("org.domco.connectfour.server.controller.application.mbean:name=playerLookupManager,type=PlayerLookupManager");

            mbsx.invoke(playerLookupManagerName, "emptyPlayerLookupMap", new Object[0], new String[0]);

        } catch (final JMException | IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
