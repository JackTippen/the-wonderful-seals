package org.domco.connectfour.server.ui.domain;

/**
 * Enum of possible game statuses.
 */
public enum GameStatus {

    /**
     * Red player has won the game.
     */
    RED_WIN,

    /**
     * Yellow player has won the game.
     */
    YELLOW_WIN,

    /**
     * Neither player has won the game yet.
     */
    UNDETERMINED,

    /**
     * There are no more available moves and no player has won.
     */
    TIE

}
