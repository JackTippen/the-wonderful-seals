package org.domco.connectfour.server.ui.stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.service.GamePlayService;
import org.domco.connectfour.server.ui.stepdefs.domainobjects.GameRowInformation;
import org.domco.connectfour.server.ui.stepdefs.domainobjects.GameStateInformation;
import org.domco.connectfour.server.ui.stepdefs.mappers.GameInformationMapper;

import javax.inject.Inject;
import java.util.List;

public class PostGameStepDefs {

    private List<GameStateInformation> gameStateInformationList;

    private final GameInformationMapper gameInformationMapper;
    private final GamePlayService gamePlayService;

    @Inject
    public PostGameStepDefs(GameInformationMapper gameInformationMapper, GamePlayService gamePlayService){
        this.gameInformationMapper = gameInformationMapper;
        this.gamePlayService = gamePlayService;
    }

    @Then("^the game has the following game state information:$")
    public void theGameIsOverWithTheFollowingGameStateInformation(List<GameStateInformation> gameStateInformationList) {
        this.gameStateInformationList = gameStateInformationList;
    }

    @And("^the following pieces were played:$")
    public void theFollowingPiecesWerePlayed(List<GameRowInformation> gameRowInformationList) {
        Game expectedGame = gameInformationMapper.mapToExpectedGame(gameStateInformationList, gameRowInformationList);
        gamePlayService.assertExpectedGameMatchesActualGame(expectedGame);

    }

}
