package org.domco.connectfour.server.ui.stepdefs.mappers;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.domco.connectfour.server.ui.constants.Constant;
import org.domco.connectfour.server.ui.domain.*;
import org.domco.connectfour.server.ui.domain.GamePiece.GamePieceBuilder;
import org.domco.connectfour.server.ui.domain.Game.GameBuilder;
import org.domco.connectfour.server.ui.domain.GamePageData.GamePageDataBuilder;
import org.domco.connectfour.server.ui.domain.GamePlayer.GamePlayerBuilder;
import org.domco.connectfour.server.ui.domain.GameState.GameStateBuilder;
import org.domco.connectfour.server.ui.domain.HomePageData.HomePageDataBuilder;
import org.domco.connectfour.server.ui.domain.UiPageData.UiPageDataBuilder;
import org.domco.connectfour.server.ui.stepdefs.domainobjects.GameRowInformation;
import org.domco.connectfour.server.ui.stepdefs.domainobjects.GameStateInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Game Information Mapper Handler.
 */
@Component
public class GameInformationMapperHandler implements GameInformationMapper {

    private static final Logger logger = LoggerFactory.getLogger(GameInformationMapperHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Game mapToExpectedGame(List<GameStateInformation> gameStateInformationList) {
        //No Game row information has been supplied in the test so a game has not begun
        Set<GamePiece> gamePieceSet = new HashSet<>();
        String waitingMessage = Constant.WAITING_MESSAGE;
        Game expectedGame = mapToExpectedGame(gameStateInformationList, gamePieceSet, waitingMessage);

        return expectedGame;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Game mapToExpectedGame (List<GameStateInformation> gameStateInformationList, List<GameRowInformation> gameRowInformationList) {

        //Create set of expected pieces for both players
        List<GamePiece> gamePieces = mapGameRowInformationList(gameRowInformationList);
        Set<GamePiece> gamePieceSet = new HashSet<>(gamePieces);
        String waitingMessage = Constant.DROP_MESSAGE;
        Game expectedGame = mapToExpectedGame(gameStateInformationList, gamePieceSet, waitingMessage);

        return expectedGame;

    }

    /**
     * @param gameStateInformationList state of the game including player information, height, width and who just played a piece
     * @param gamePieceSet expected set of game pieces
     * @return expected game object for test assertion
     */
    private Game mapToExpectedGame(List<GameStateInformation> gameStateInformationList, Set<GamePiece> gamePieceSet,
                                   String waitingMessage) {

        logger.debug("Attempting to map game state information and game row information to the expected game object ");

        //Create expectedHomePageData for both players
        //Fields on the home and game page object that are constant and do not change.
        String homeAndGamePageTitle = Constant.TITLE_TEXT;
        String message = waitingMessage;

        HomePageData expectedHomePageData = new HomePageDataBuilder()
                .withConnectFourTitle(homeAndGamePageTitle)
                .build();

        //Create expectedYellowGamePageData
        GamePageData expectedYellowGamePageData = new GamePageDataBuilder()
                .withConnectFourTitle(homeAndGamePageTitle)
                .withWaitingMessage(message)
                //Don't build player Id as don't know the guid that is randomly generated
                .build();

        //Create expectedYellowUiDataPage
        UiPageData expectedYellowUiPageData = new UiPageDataBuilder()
                .withHomePageData(expectedHomePageData)
                .withGamePageData(expectedYellowGamePageData)
                .build();

        //Create expectedRedGamePageData
        GamePageData expectedRedGamePageData = new GamePageDataBuilder()
                .withConnectFourTitle(homeAndGamePageTitle)
                .withWaitingMessage(message)
                //Don't build player Id as don't know the guid that is randomly generated
                .build();

        //Create expectedRedUiDataPage
        UiPageData expectedRedUiPageData = new UiPageDataBuilder()
                .withHomePageData(expectedHomePageData)
                .withGamePageData(expectedRedGamePageData)
                .build();

        //Create expected game state for both players - This should be the same
        PlayerColour colourJustPlayed = mapColourJustPlayed(gameStateInformationList.get(0));

        GameState expectedGameState = new GameStateBuilder()
                .withGameHeight(gameStateInformationList.get(0).getGameHeight())
                .withGameWidth(gameStateInformationList.get(0).getGameWidth())
                .withGamePieceSet(gamePieceSet)
                .withColourPlayed(colourJustPlayed)
                .build();

        //Create expected player object for all players in the test
        List<GamePlayer> expectedPlayers = new ArrayList<>();

        for(GameStateInformation currentGameStateInfo : gameStateInformationList){

            UiPageData expectedUiPageData;
            if (StringUtils.equals(currentGameStateInfo.getPlayerColour(), Constant.YELLOW) ||
                   StringUtils.equals(currentGameStateInfo.getPlayerColour(), Constant.UNDEFINED)) {
                expectedUiPageData = expectedYellowUiPageData;
            } else if (StringUtils.equals(currentGameStateInfo.getPlayerColour(), Constant.RED)) {
                expectedUiPageData = expectedRedUiPageData;
            } else {
                throw new IllegalStateException("Player colour was not defined as either " + Constant.RED + " , "
                        + Constant.YELLOW + " or " + Constant.UNDEFINED);
            }

            PlayerColour playerColour = mapPlayerColour(currentGameStateInfo);
            GameStatus playerGameStatus = getGameStatus(currentGameStateInfo);

            GamePlayer expectedPlayer = new GamePlayerBuilder()
                    .withPlayerName(currentGameStateInfo.getPlayerName())
                    .withPlayerColour(playerColour)
                    .withOpponentName(currentGameStateInfo.getOpponentName())
                    .withGameState(expectedGameState)
                    .withGameStatus(playerGameStatus)
                    .withUiPageData(expectedUiPageData)
                    .build();

            expectedPlayers.add(expectedPlayer);

        }

        //create expected game
        Game expectedGame = new GameBuilder()
                .withGamePlayerList(expectedPlayers)
                .build();

        logger.debug("Returning mapped expected game object.");
        return expectedGame;
    }

    /**
     * Maps the player colour string from the table to the PlayerColour enum
     *
     * @param gameStateInformation player information as specified in the player information table in the feature file
     * @return PlayerColour enum
     */
    private PlayerColour mapPlayerColour(GameStateInformation gameStateInformation) {
        PlayerColour playerColour;
        if (StringUtils.equals(gameStateInformation.getPlayerColour(), Constant.YELLOW)) {
            playerColour = PlayerColour.YELLOW;
        } else if (StringUtils.equals(gameStateInformation.getPlayerColour(), Constant.RED)) {
            playerColour = PlayerColour.RED;
        } else if (StringUtils.equals(gameStateInformation.getPlayerColour(), Constant.UNDEFINED)) {
            playerColour = PlayerColour.UNDEFINED;
        } else {
            logger.debug("playerColour was not entered as " + Constant.RED + ", " + Constant.YELLOW + " or " + Constant.UNDEFINED
                    + " in the player information table");
            throw new IllegalStateException("playerColour was not specified using the correct format in the player information table");
        }
        return playerColour;
    }

    /**
     * Maps the game status string from the table to the GameStatus enum
     *
     * @param gameStateInformation player information as specified in the player information table in the feature file
     * @return GameStatus enum
     */
    private GameStatus getGameStatus(GameStateInformation gameStateInformation) {
        GameStatus playerGameStatus;
        if (StringUtils.equals(gameStateInformation.getGameStatus(), Constant.YELLOW_WIN)) {
            playerGameStatus = GameStatus.YELLOW_WIN;
        } else if (StringUtils.equals(gameStateInformation.getGameStatus(), (Constant.RED_WIN))) {
            playerGameStatus = GameStatus.RED_WIN;
        } else if (StringUtils.equals(gameStateInformation.getGameStatus(), Constant.UNDETERMINED)) {
            playerGameStatus = GameStatus.UNDETERMINED;
        } else if (StringUtils.equals(gameStateInformation.getGameStatus(), Constant.TIE)) {
            playerGameStatus = GameStatus.TIE;
        } else {
            logger.debug("Game status was not entered as " + Constant.RED_WIN +", " + Constant.YELLOW_WIN + ", "
                    + Constant.UNDETERMINED +" or " + Constant.TIE+ " in the player information table");
            throw new IllegalStateException("gameStatus was not specified using the correct format in the player information table");
        }
        return playerGameStatus;
    }

    /**
     * Maps the colour just played string from the table to the PlayerColour enum
     *
     * @param gameStateInformation game state information as specified in the game state information table in the feature file
     * @return PlayerColour enum
     */
    private PlayerColour mapColourJustPlayed(GameStateInformation gameStateInformation) {
        PlayerColour colourJustPlayed;
        if (StringUtils.equals(gameStateInformation.getColourJustPlayed(), Constant.RED)) {
            colourJustPlayed = PlayerColour.RED;
        } else if (StringUtils.equals(gameStateInformation.getColourJustPlayed(), Constant.YELLOW)) {
            colourJustPlayed = PlayerColour.YELLOW;
        } else {
            logger.debug("The game as not begun so no colour has been played");
            colourJustPlayed = PlayerColour.UNDEFINED;
        }
        return colourJustPlayed;
    }

    /**
     * Maps the list of game row information objects into a list of game pieces
     *
     * @param gameRowInformationList list of all pieces per row in string format
     * @return gamePieces             the list to contain all game pieces that have been specified
     * in the game grid table in the feature file
     */
    private List<GamePiece> mapGameRowInformationList(List<GameRowInformation> gameRowInformationList) {

        List<GamePiece> allGamePieces = new ArrayList<>();

        //The table starts with the top row. But we want to iterate from the bottom of the game grid upwards.
        List<GameRowInformation> rowsInCorrectOrder = ImmutableList.copyOf(gameRowInformationList).reverse();

        //Iterate through each GameInformationRow
        for (int rowNum = 0; rowNum < rowsInCorrectOrder.size(); rowNum++) {

            GameRowInformation currentRow = rowsInCorrectOrder.get(rowNum);

            //Iterate through each column on the current row
            for (int columnNum = 0; columnNum < currentRow.fetchColumnPiecesForRow().size(); columnNum++) {

                String currentColumn = currentRow.fetchColumnPiecesForRow().get(columnNum);

                PlayerColour playerColour;
                //Only create a gamePiece if there was actually something in this table cell
                if (!StringUtils.equals(currentColumn, "")) {

                    if (StringUtils.equals(currentColumn, Constant.RED_COLUMN_SYMBOL)) {
                        playerColour = PlayerColour.RED;
                    } else if (StringUtils.equals(currentColumn, Constant.YELLOW_COLUMN_SYMBOL)) {
                        playerColour = PlayerColour.YELLOW;
                    } else {
                        logger.debug("Player colour was not entered as a " + Constant.RED_COLUMN_SYMBOL +
                                " or a " + Constant.YELLOW_COLUMN_SYMBOL + " in the game piece table");
                        throw new IllegalStateException("Illegal character used in game piece table");
                    }

                    //Create a game piece for this table cell entry and add to the GamePiece list
                    GamePiece gp = new GamePieceBuilder()
                            .withxPosition(columnNum)
                            .withyPosition(rowNum)
                            .withPieceColour(playerColour)
                            .build();

                    allGamePieces.add(gp);

                } else {
                    logger.debug("This table cell was empty");
                }
            }
        }
        return allGamePieces;
    }

}
