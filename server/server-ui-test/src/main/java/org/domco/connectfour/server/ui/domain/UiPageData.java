package org.domco.connectfour.server.ui.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Ui Page Data object
 */
public class UiPageData {

    private final GamePageData gamePageData;
    private HomePageData homePageData;

    private UiPageData(UiPageDataBuilder builder) {
        this.gamePageData = builder.gamePageData;
        this.homePageData = builder.homePageData;
    }

    public GamePageData getGamePageData() {
        return gamePageData;
    }

    public HomePageData getHomePageData() {
        return homePageData;
    }

    public void setHomePageData(HomePageData homePageData) {
        this.homePageData = homePageData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        UiPageData that = (UiPageData) o;

        return new EqualsBuilder()
                .append(gamePageData, that.gamePageData)
                .append(homePageData, that.homePageData)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(gamePageData)
                .append(homePageData)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("gamePageData", gamePageData)
                .append("homePageData", homePageData)
                .toString();
    }

    /**
     * Builder for the UiPageDataBuilder
     */
    public static class UiPageDataBuilder {

        private GamePageData gamePageData;
        private HomePageData homePageData;

        public UiPageData build() {
            return new UiPageData(this);
        }

        public UiPageDataBuilder withGamePageData(final GamePageData gamePageData) {
            this.gamePageData = gamePageData;
            return this;
        }

        public UiPageDataBuilder withHomePageData(final HomePageData homePageData) {
            this.homePageData = homePageData;
            return this;
        }

    }

}
