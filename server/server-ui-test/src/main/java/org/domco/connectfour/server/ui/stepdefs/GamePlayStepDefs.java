package org.domco.connectfour.server.ui.stepdefs;

import com.frameworkium.core.ui.tests.BaseUITest;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;
import org.domco.connectfour.server.ui.domain.PlayerColour;
import org.domco.connectfour.server.ui.service.GamePlayService;
import org.domco.connectfour.server.ui.service.TestStateContainer;

import javax.inject.Inject;

/**
 * Step definitions gor Game Play.
 */
public class GamePlayStepDefs extends BaseUITest {

    private TestStateContainer testStateContainer;
    private GamePlayService gamePlayService;

    @Inject
    public GamePlayStepDefs(TestStateContainer testStateContainer, GamePlayService gamePlayService) {
        this.testStateContainer = testStateContainer;
        this.gamePlayService = gamePlayService;
    }

    @When("^\"([^\"]*)\" drops a piece in column (\\d+)$")
    public void dropsAPieceInColumn(String playerName, int columnNum) {

        String yellowPlayerName = testStateContainer.getGame().getGamePlayerList().get(0).getPlayerName();
        String redPlayerName = testStateContainer.getGame().getGamePlayerList().get(1).getPlayerName();

        PlayerColour playerToPlayPiece;
        if (StringUtils.equals(playerName, redPlayerName)) {

            playerToPlayPiece = PlayerColour.RED;

        } else if (StringUtils.equals(playerName, yellowPlayerName)) {

            playerToPlayPiece = PlayerColour.YELLOW;

        } else {
            throw new IllegalStateException("The player name did not equal " + redPlayerName + " or " + yellowPlayerName);
        }

        gamePlayService.dropPieceInColumn(playerToPlayPiece, columnNum);

    }

}
