package org.domco.connectfour.server.ui.presentation.service;

import com.frameworkium.core.ui.pages.PageFactory;
import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.domco.connectfour.server.ui.domain.HomePageData;
import org.domco.connectfour.server.ui.domain.PlayerColour;
import org.domco.connectfour.server.ui.presentation.domainobjects.HomePageObject;
import org.domco.connectfour.server.ui.presentation.mappers.GamePagePresentationMapper;
import org.domco.connectfour.server.ui.presentation.mappers.HomePagePresentationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

/**
 * Presentation service for the Game Initiation Service
 */
@Component
public class GameInitiationPresentationServiceHandler implements GameInitiationPresentationService {


    private HomePagePresentationMapper homePagePresentationMapper;

    private GamePagePresentationMapper gamePagePresentationMapper;

    private static final Logger logger = LoggerFactory.getLogger(GameInitiationPresentationServiceHandler.class);

    private BrowserInteractionService browserInteractionService;

    @Inject
    public GameInitiationPresentationServiceHandler(HomePagePresentationMapper homePagePresentationMapper,
                                                    GamePagePresentationMapper gamePagePresentationMapper,
                                                    BrowserInteractionService browserInteractionService) {

        this.homePagePresentationMapper = homePagePresentationMapper;
        this.gamePagePresentationMapper = gamePagePresentationMapper;
        this.browserInteractionService = browserInteractionService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HomePageData openHomePage(GamePlayer gamePlayer) {

        logger.debug("Navigating to the Home Page in the browser.");

        //If this is the red player a new tab needs to be opened as yellow is waiting to play
        if (gamePlayer.getPlayerColour().equals(PlayerColour.RED)) {
            browserInteractionService.newBrowserTab();
            browserInteractionService.switchTabFocus(PlayerColour.RED);
        }
        HomePageData homePageData = homePagePresentationMapper.mapToHomeDto();

        logger.debug("Returning homePageData object.");

        return homePageData;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GamePlayer requestToPlayAGame(GamePlayer gamePlayer) {

        logger.debug("Player entering their user information to register for a game.");

        registerPlayerForGame(gamePlayer);

        GamePlayer updatedGamePlayer = gamePagePresentationMapper.mapToGamePlayer(gamePlayer);

        //Give the Updated Game Player object the HomePageData
        updatedGamePlayer.getUiPageData().setHomePageData(gamePlayer.getUiPageData().getHomePageData());

        logger.debug("Returning the updated Game Player from the Game Page Object after the user has registered to play.");
        return updatedGamePlayer;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GamePlayer> bothPlayersJoinToInitiateGame(GamePlayer yellowGamePlayer, GamePlayer redGamePlayer) {

        logger.debug("Initiating the game for both players");

        browserInteractionService.switchTabFocus(PlayerColour.YELLOW);
        //Yellow player's game page is waiting until a second player joins
        GamePlayer waitingYellowPlayer = requestToPlayAGame(yellowGamePlayer);

        browserInteractionService.switchTabFocus(PlayerColour.RED);
        //Red player's game page data is in the game straight away as yellow was already waiting
        GamePlayer updatedRedPlayer = requestToPlayAGame(redGamePlayer);

        //Update yellow player's gameDto now the game has begun for both players
        browserInteractionService.switchTabFocus(PlayerColour.YELLOW);
        GamePlayer updatedYellowPlayer = gamePagePresentationMapper.mapToGamePlayer(yellowGamePlayer);

        //Give updatedYellowPlayer the home page data
        updatedYellowPlayer.getUiPageData().setHomePageData(waitingYellowPlayer.getUiPageData().getHomePageData());

        List<GamePlayer> updatedGamePlayers = Arrays.asList(updatedYellowPlayer, updatedRedPlayer);

        logger.debug("Returning both updated players");

        return updatedGamePlayers;

    }

    /**
     * Registers player for game by using the page object to interact with the UI
     *
     * @param gamePlayer player being registered to join the game
     */
    private void registerPlayerForGame(GamePlayer gamePlayer) {
        PageFactory.newInstance(HomePageObject.class)
                .getNewGameForm()
                .registerPlayerForGame(gamePlayer.getPlayerName());
    }

}
