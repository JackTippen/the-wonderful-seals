package org.domco.connectfour.server.ui.presentation.service;

import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.domco.connectfour.server.ui.domain.HomePageData;
import org.openqa.selenium.WebDriver;

import java.util.List;

/**
 * Interface for the Game Initiation Presentation Service.
 */
public interface GameInitiationPresentationService {

    /**
     * Opens the Connect4 home page in the browser
     *
     * @param gamePlayer that is opening the home page
     * @return returns the home domain object
     */
    HomePageData openHomePage(GamePlayer gamePlayer);

    /**
     * Inputs the player's name into the text area and selects to play a game
     *
     * @param gamePlayer who is going to join the game
     * @return Updated GamePlayer object
     */
    GamePlayer requestToPlayAGame(GamePlayer gamePlayer);

    /**
     * Inputs the opponent's name into the text area and selects to play a game.
     * Since two players are waiting to play the game the game page is displayed and the game begins
     *
     * @param yellowGamePlayer player who is going to join as the first person in the game.
     * @param redGamePlayer opponent player who is going to join and initiate the game.
     * @return updated player objects now that the game has begun for both players
     */
    List<GamePlayer> bothPlayersJoinToInitiateGame(GamePlayer yellowGamePlayer, GamePlayer redGamePlayer);


}
