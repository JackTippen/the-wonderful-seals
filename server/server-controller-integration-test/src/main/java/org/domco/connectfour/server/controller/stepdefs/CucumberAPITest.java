package org.domco.connectfour.server.controller.stepdefs;

import cucumber.api.java.Before;
import org.domco.connectfour.server.controller.integration.ApiControllerIntegration;

import javax.inject.Inject;

/**
 * Setup class for API tests to remove test data from previous tests.
 */
public class CucumberAPITest {

    private final ApiControllerIntegration apiControllerIntegration;

    @Inject
    public CucumberAPITest(ApiControllerIntegration apiControllerIntegration) {
        this.apiControllerIntegration = apiControllerIntegration;
    }

    @Before
    public void setup() {
        apiControllerIntegration.deleteAllPlayers();
    }

}
