package org.domco.connectfour.server.controller.stepdefs.mappers;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.domco.connectfour.server.controller.constants.Constant;
import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.domain.Game.GameBuilder;
import org.domco.connectfour.server.controller.domain.GamePiece;
import org.domco.connectfour.server.controller.domain.GamePiece.GamePieceBuilder;
import org.domco.connectfour.server.controller.domain.Player;
import org.domco.connectfour.server.controller.domain.Player.PlayerBuilder;
import org.domco.connectfour.server.controller.domain.PlayerGameState;
import org.domco.connectfour.server.controller.domain.PlayerGameState.GameStateBuilder;
import org.domco.connectfour.server.controller.domain.GameStatus;
import org.domco.connectfour.server.controller.domain.PlayerColour;
import org.domco.connectfour.server.controller.service.APITestStateContainer;
import org.domco.connectfour.server.controller.stepdefs.domainobjects.GameRowInformation;
import org.domco.connectfour.server.controller.stepdefs.domainobjects.GameStateInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Game Information Mapper Handler.
 */
@Component
public class GameStateInformationMapperHandler implements GameStateInformationMapper {

    private static final Logger logger = LoggerFactory.getLogger(GameStateInformationMapperHandler.class);

    private final APITestStateContainer apiTestStateContainer;

    @Inject
    public GameStateInformationMapperHandler (APITestStateContainer apiTestStateContainer) {
        this.apiTestStateContainer = apiTestStateContainer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Game mapToExpectedGame(List<GameStateInformation> gameStateInformationList) {

        //No Game row information has been supplied in the test so a game has not begun
        Set<GamePiece> gamePieceSet = new HashSet<>();
        Game expectedGame = mapToExpectedGame(gameStateInformationList, gamePieceSet);

        return expectedGame;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Game mapToExpectedGame(List<GameStateInformation> gameStateInformationList, List<GameRowInformation> gameRowInformationList) {

        //Create a set of expected pieces for both players
        List<GamePiece> gamePieces = mapGameRowInformationList(gameRowInformationList);
        Set<GamePiece> gamePieceSet = new HashSet<>(gamePieces);
        Game expectedGame = mapToExpectedGame(gameStateInformationList, gamePieceSet);

        return expectedGame;

    }

    /**
     * @param gameStateInformationList state of the game including player information, height, width and who just played a piece
     * @param gamePieceSet             expected set of game pieces
     * @return expected game state object for test assertion
     */
    private Game mapToExpectedGame(List<GameStateInformation> gameStateInformationList, Set<GamePiece> gamePieceSet) {

        logger.debug("Attempting to map game state information and game row information to the expected game state object");

        List<Player> expectedPlayers = new ArrayList<>();
        List<PlayerGameState> expectedPlayerGameStates = new ArrayList<>();

        for (int i = 0; i < gameStateInformationList.size(); i++) {

            //PlayerId is randomly generated. Therefore it must be taken out of this assertion.
            //It's format is explicitly tested in "PlayerCanJoinGame.feature"
            Player player = new PlayerBuilder()
                    .withPlayerName(gameStateInformationList.get(i).getPlayerName())
                    .withPlayerId(apiTestStateContainer.getGame().getPlayers().get(i).getPlayerId())
                    .build();
            expectedPlayers.add(player);

            //If the gameStatus is UNDETERMINED then the API returned the winner as null.
            //Therefore, it the feature file doesn't specify a winner the empty string will be mapped to null.
            String winner = null;
            if (StringUtils.isNotEmpty(gameStateInformationList.get(i).getWinner())){
                winner = gameStateInformationList.get(i).getWinner();
            }

            PlayerGameState playerGameState = new GameStateBuilder()
                    .withPlayerName(gameStateInformationList.get(i).getPlayerName())
                    .withOpponentName(gameStateInformationList.get(i).getOpponentName())
                    .withPlayerColour(mapColourString(gameStateInformationList.get(i).getPlayerColour()))
                    .withOpponentColour(mapColourString(gameStateInformationList.get(i).getOpponentColour()))
                    .withTurnColour(mapColourString(gameStateInformationList.get(i).getTurnColour()))
                    .withGameStatus(mapGameStatusString(gameStateInformationList.get(i).getGameStatus()))
                    .withWinner(winner)
                    .withGameWidth(gameStateInformationList.get(i).getGameWidth())
                    .withGameHeight(gameStateInformationList.get(i).getGameHeight())
                    .withGamePieceSet(gamePieceSet)
                    .build();

            expectedPlayerGameStates.add(playerGameState);
        }

        Game expectedGame = new GameBuilder()
                .withPlayers(expectedPlayers)
                .withPlayerGameStateList(expectedPlayerGameStates)
                .build();

        logger.debug("Returning mapped game state objects");
        return expectedGame;

    }

    /**
     * Maps the gameStatus string into a GameStatus enum
     *
     * @param gameStatusString the string value of the expected game status passed in from the feature file
     * @return Corresponding GameStatus enum
     */
    private GameStatus mapGameStatusString(String gameStatusString) {
        GameStatus gameStatus;
        if (StringUtils.equals(gameStatusString, Constant.YELLOW_WIN)) {
            gameStatus = GameStatus.YELLOW_WIN;
        } else if (StringUtils.equals(gameStatusString, Constant.RED_WIN)) {
            gameStatus = GameStatus.RED_WIN;
        } else if (StringUtils.equals(gameStatusString, Constant.TIE)) {
            gameStatus = GameStatus.TIE;
        } else if (StringUtils.equals(gameStatusString, Constant.UNDETERMINED)) {
            gameStatus = GameStatus.UNDETERMINED;
        } else {
            throw new IllegalStateException("The provided game status was not one of the allowed statuses:" +
                    Constant.RED_WIN + ", " + Constant.YELLOW_WIN + ", " + Constant.TIE + " or " + Constant.UNDETERMINED);
        }

        return gameStatus;
    }

    /**
     * Maps the playerColour string into a PlayerColour enum
     *
     * @param colour the string value of the expected player colour passed in from the feature file
     * @return corresponding PlayerColour enum
     */
    private PlayerColour mapColourString(String colour) {
        PlayerColour playerColour;
        if (StringUtils.equals(colour, Constant.RED)) {
            playerColour = PlayerColour.RED;
        } else if (StringUtils.equals(colour, Constant.YELLOW)) {
            playerColour = PlayerColour.YELLOW;
        } else {
            throw new IllegalStateException("The provided colour was neither " + Constant.RED + "or " + Constant.YELLOW);
        }
        return playerColour;
    }

    /**
     * Maps the list of game row information objects into a list of game pieces
     *
     * @param gameRowInformationList list of all pieces per row in string format
     * @return gamePieces             the list to contain all game pieces that have been specified
     * in the game grid table in the feature file
     */
    private List<GamePiece> mapGameRowInformationList(List<GameRowInformation> gameRowInformationList) {

        List<GamePiece> allGamePieces = new ArrayList<>();

        //The table starts with the top row. But we want to iterate from the bottom of the game grid upwards.
        List<GameRowInformation> rowsInCorrectOrder = ImmutableList.copyOf(gameRowInformationList).reverse();

        //Iterate through each GameInformationRow
        for (int rowNum = 0; rowNum < rowsInCorrectOrder.size(); rowNum++) {

            GameRowInformation currentRow = rowsInCorrectOrder.get(rowNum);

            //Iterate through each column on the current row
            for (int columnNum = 0; columnNum < currentRow.fetchColumnPiecesForRow().size(); columnNum++) {

                String currentColumn = currentRow.fetchColumnPiecesForRow().get(columnNum);

                PlayerColour playerColour;
                //Only create a gamePiece if there was actually something in this table cell
                if (!StringUtils.equals(currentColumn, "")) {

                    if (StringUtils.equals(currentColumn, Constant.RED_COLUMN_SYMBOL)) {
                        playerColour = PlayerColour.RED;
                    } else if (StringUtils.equals(currentColumn, Constant.YELLOW_COLUMN_SYMBOL)) {
                        playerColour = PlayerColour.YELLOW;
                    } else {
                        logger.debug("Player colour was not entered as a " + Constant.RED_COLUMN_SYMBOL +
                                " or a " + Constant.YELLOW_COLUMN_SYMBOL + " in the game piece table");
                        throw new IllegalStateException("Illegal character used in game piece table");
                    }

                    //Create a game piece for this table cell entry and add to the GamePiece list
                    GamePiece gp = new GamePieceBuilder()
                            .withxPosition(columnNum)
                            .withyPosition(rowNum)
                            .withPieceColour(playerColour)
                            .build();

                    allGamePieces.add(gp);

                } else {
                    logger.debug("This table cell was empty");
                }
            }
        }
        return allGamePieces;
    }

}
