package org.domco.connectfour.server.controller.stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.domain.Player;
import org.domco.connectfour.server.controller.service.APITestStateContainer;
import org.domco.connectfour.server.controller.service.ApiControllerService;
import org.domco.connectfour.server.controller.stepdefs.domainobjects.GameRowInformation;
import org.domco.connectfour.server.controller.stepdefs.domainobjects.GameStateInformation;
import org.domco.connectfour.server.controller.stepdefs.mappers.GameStateInformationMapper;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step definitions for the API Controller feature file tests
 */
public class ApiControllerStepDefs {

    private List<GameStateInformation> gameStateInformationList;

    private ApiControllerService apiControllerService;
    private GameStateInformationMapper gameStateInformationMapper;
    private APITestStateContainer apiTestStateContainer;

    @Inject
    public ApiControllerStepDefs(ApiControllerService apiControllerService, GameStateInformationMapper gameStateInformationMapper,
                                 APITestStateContainer apiTestStateContainer) {
        this.apiControllerService = apiControllerService;
        this.gameStateInformationMapper = gameStateInformationMapper;
        this.apiTestStateContainer = apiTestStateContainer;
    }

    @When("^\"([^\"]*)\" tries to join a new game$")
    public void triesToJoinANewGame(String playerName) {
        List<Player> players = new ArrayList<>();
        Player player = apiControllerService.playerRequestsToJoinGame(playerName);
        players.add(player);
        //Store the state of the actual game in the apiTestStateContainer
        apiTestStateContainer.setGame(apiControllerService.retrievePlayersGameStates(players));
    }

    @Given("^two users \"([^\"]*)\" and \"([^\"]*)\" have chosen to join a game$")
    public void twoUsersAndHaveChosenToJoinAGame(String yellowPlayer, String redPlayer) {
        List<Player> players = new ArrayList<>();
        players.add(apiControllerService.playerRequestsToJoinGame(yellowPlayer));
        players.add(apiControllerService.playerRequestsToJoinGame(redPlayer));
        //Store the state of the actual game in the apiTestStateContainer
        apiTestStateContainer.setGame(apiControllerService.retrievePlayersGameStates(players));
    }

    @When("^\"([^\"]*)\" drops a piece in column (\\d+)$")
    public void dropsAPieceInColumn(String playerName, int columnNum) {
        apiControllerService.playGamePiece(playerName, columnNum);
    }

    @Then("^\"([^\"]*)\" is given a player ID$")
    public void givenAPlayerID(String playerName) {
        Optional<Player> playerOptional = apiTestStateContainer.getGame().getPlayers().stream()
                .filter(player -> player.getPlayerName().equals(playerName))
                .findFirst();

        if (playerOptional.isPresent()) {
            final Player currentPlayer = playerOptional.get();
            apiControllerService.assertPlayerIdIsPresent(currentPlayer);
        } else {
            throw new IllegalStateException(playerName + " was not found in the list of players.");
        }

    }

    @Then("^a game is created for the players with the following states$")
    public void aGameIsCreatedForThePlayersWithTheFollowingState(List<GameStateInformation> expectedGameStateInformationList) {

        this.gameStateInformationList = expectedGameStateInformationList;
        //Store the state of the actual game in the apiTestStateContainer
        apiTestStateContainer.setGame(apiControllerService.retrievePlayersGameStates(apiTestStateContainer.getGame().getPlayers()));

    }

    @And("^the following pieces were played:$")
    public void theFollowingPiecesWerePlayed(List<GameRowInformation> gameRowInformationList) {
        Game expectedGame = gameStateInformationMapper.mapToExpectedGame(gameStateInformationList, gameRowInformationList);
        apiControllerService.assertPlayerGameStates(expectedGame);
    }
}
