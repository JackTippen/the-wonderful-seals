package org.domco.connectfour.server.controller.service;

import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.domain.Player;

import java.util.List;

/**
 * Interface for API Controller Service
 */
public interface ApiControllerService {

    /**
     * Creates playerId for a given player by making them join a game
     *
     * @param playerName name of the player to join the game and receive the playerId
     * @return The player that has just joined the game and received the playerId
     */
    Player playerRequestsToJoinGame(String playerName);

    /**
     * For all players in the game retrieve their PlayerGameState
     *
     * @param players List of the players in the game
     * @return Game object containing all PlayerGameStates
     */
    Game retrievePlayersGameStates(List<Player> players);

    /**
     * Plays a piece in the chosen column
     *
     * @param playerName name of the player who is attempting to perform the move
     * @param columnNum  number of the column the player wants to play their piece in
     */
    void playGamePiece(String playerName, int columnNum);

    /**
     * Checks that the actual state of the game is equal to our expected game state
     *
     * @param expectedGame the expected state of the game after the test.
     */
    void assertPlayerGameStates(Game expectedGame);

    /**
     * Takes the player Id from the player parameter and checks it matches the UUID format.
     *
     * @param currentPlayer player that has requested to join a game.
     */
    void assertPlayerIdIsPresent(Player currentPlayer);
}
