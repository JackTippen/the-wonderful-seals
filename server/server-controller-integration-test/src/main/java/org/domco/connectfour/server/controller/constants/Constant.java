package org.domco.connectfour.server.controller.constants;

/**
 * Constants class.
 */
public class Constant {

    public final static String RED = "red";
    public final static String YELLOW = "yellow";
    public final static String UNDEFINED = "undefined";

    public final static String YELLOW_WIN = "yellowWin";
    public final static String RED_WIN = "redWin";
    public final static String UNDETERMINED = "undetermined";
    public final static String TIE = "tie";

    public final static String RED_COLUMN_SYMBOL = "R";
    public final static String YELLOW_COLUMN_SYMBOL = "Y";

}
