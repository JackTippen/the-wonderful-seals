package org.domco.connectfour.game.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Game Piece
 */
public final class GamePiece {

    private final int xPosition;

    private final int yPosition;

    private final PieceColour pieceColour;

    private GamePiece(final GamePieceBuilder builder) {
        this.xPosition = builder.xPosition;
        this.yPosition = builder.yPosition;
        this.pieceColour = builder.pieceColour;
    }

    public PieceColour getPieceColour() {
        return pieceColour;
    }

    public int getxPosition() {
        return xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(xPosition)
                .append(yPosition)
                .append(pieceColour)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals = false;
        if (obj instanceof GamePiece) {
            final GamePiece rhs = (GamePiece) obj;
            equals = new EqualsBuilder().append(xPosition, rhs.xPosition)
                    .append(yPosition, rhs.yPosition)
                    .append(pieceColour, rhs.pieceColour)
                    .isEquals();
        }
        return equals;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("xPosition", xPosition)
                .append("yPosition", yPosition)
                .append("pieceColour", pieceColour)
                .toString();
    }

    /**
     * Builder for the GamePiece class
     */
    public static class GamePieceBuilder {

        private int xPosition;

        private int yPosition;

        private PieceColour pieceColour;

        public GamePiece build() {
            return new GamePiece(this);
        }

        public GamePieceBuilder withxPosition(final int xPosition) {
            this.xPosition = xPosition;
            return this;
        }

        public GamePieceBuilder withyPosition(final int yPosition) {
            this.yPosition = yPosition;
            return this;
        }

        public GamePieceBuilder withPieceColour(final PieceColour pieceColour) {
            this.pieceColour = pieceColour;
            return this;
        }

    }
}
