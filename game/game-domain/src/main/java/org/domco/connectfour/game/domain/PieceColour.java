package org.domco.connectfour.game.domain;

/**
 * Enum of connect 4 piece colours.
 */
public enum PieceColour {

    /**
     * Piece is red.
     */
    RED,

    /**
     * Piece is yellow.
     */
    YELLOW
}
