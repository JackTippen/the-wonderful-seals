package org.domco.connectfour.game.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Game Move
 */
public class GameMove {

    private final GameState startGameState;

    private final GameState endGameState;

    private final GamePiece playedPiece;

    private GameMove(final GameMoveBuilder builder) {
        this.startGameState = builder.startGameState;
        this.endGameState = builder.endGameState;
        this.playedPiece = builder.playedPiece;
    }

    public GameState getStartGameState() {
        return startGameState;
    }

    public GameState getEndGameState() {
        return endGameState;
    }

    public GamePiece getPlayedPiece() {
        return playedPiece;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(startGameState)
                .append(endGameState)
                .append(playedPiece)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final GameMove gameMove = (GameMove) o;

        return new EqualsBuilder()
                .append(startGameState, gameMove.startGameState).append(endGameState, gameMove.endGameState)
                .append(playedPiece, gameMove.playedPiece)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("startGameState", startGameState)
                .append("endGameState", endGameState)
                .append("playedPiece", playedPiece)
                .toString();
    }

    /**
     * Builder for the GameMove class
     */
    public static class GameMoveBuilder {

        private GameState startGameState;

        private GameState endGameState;

        private GamePiece playedPiece;

        public GameMove build() {
            return new GameMove(this);
        }

        public GameMoveBuilder withStartGameState(final GameState startGameState) {
            this.startGameState = startGameState;
            return this;
        }

        public GameMoveBuilder withEndGameState(final GameState endGameState) {
            this.endGameState = endGameState;
            return this;
        }

        public GameMoveBuilder withPlayedPiece(final GamePiece playedPiece) {
            this.playedPiece = playedPiece;
            return this;
        }


    }
}